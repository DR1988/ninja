const path = require('path');
const webpack = require('webpack')

module.exports = {
  entry: './start.js',
  devtool: "source-map",
  output: {
    filename: 'index.js',
    // path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
};