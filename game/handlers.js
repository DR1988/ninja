import elemets from './elements'
import init from './initialize'

let { moves, undo, redo } = init
const { cells, undoBtn, redoBtn, restartBtn, wonMessage, wonTitle } = elemets
let finish=null
const winCombination = [
  [0,1,2],
  [3,4,5],
  [6,7,8],
  [0,3,6],
  [1,4,7],
  [2,5,8],
  [0,4,8],
  [2,4,6],
]
let winCombination2 = new Map()
winCombination.forEach((el, idx) => {
  switch (idx) {
    case 0:
    winCombination2.set(el,'h-top')
      break;
    case 1:
    winCombination2.set(el,'h-mid')
      break;
    case 2:
    winCombination2.set(el,'h-bot')
      break;
    case 3:
    winCombination2.set(el,'v-left')
      break;
    case 4:
    winCombination2.set(el,'v-mid')
      break;
    case 5:
    winCombination2.set(el,'v-rigth')
      break;
    case 6:
    winCombination2.set(el,'lr-diag')
      break;
    case 7:
    winCombination2.set(el,'rl-diag')
      break;
    default:
      break;
  }
})


const checkWinner = (hash, gamer) => {
  let checked  = []
  for (let i = 0; i < winCombination.length; i++) {
    checked=[]
    for (let j = 0; j < hash[gamer].length; j++) {
      for (let k = 0; k < winCombination[i].length; k++) {
          winCombination[i][k] === +hash[gamer][j] ? checked.push(+hash[gamer][j]) : null
      }
    }
    if(checked.length===3) {
      return {
        winner: gamer,
        winnerCombination: winCombination[i],
      }
      break
    }
  }
  return null
}

const winnerCalc = () => {
  if(moves.length === 9) {
    return {
      winner: null
    }
  }
  const hash = {
    ch: [],
    r: [],
  }
  moves.forEach(move => {
    if (move.classList.contains('ch')) {
      hash.ch.push(move.dataset['id']) 
    } else {
      hash.r.push(move.dataset['id']) 
    }
  })
  let winner = checkWinner(hash, 'ch')
  if(winner) return winner
  winner = checkWinner(hash, 'r')
  if(winner) return winner
  return null
}

const cellCliker = (cell, idx) => {
  if (!moves.length) {
    undoBtn.removeAttribute('disabled')
  }
  if(moves.indexOf(idx) === -1) {
    undo=[]
    redo=[]
    redoBtn.setAttribute('disabled', 'disabled')
    if(moves.length%2) {
      cell.classList.add('r')
    } else {
      cell.classList.add('ch')
    }
    moves.push(cell)
    const winner = winnerCalc()
    if(winner) return winner
    return null
  }
}

const moveForward = () => {
  const cell = undo.pop()
  if(moves.length%2) {
    cell.classList.add('r')
  } else {
    cell.classList.add('ch')
  }
  moves.push(cell)
  undoBtn.removeAttribute('disabled')
  if(!undo.length) {
    redoBtn.setAttribute('disabled', 'disabled')
    return
  }
}

const moveBack = () => {
  const cell = moves.pop()
  if(moves.length%2) {
    cell.classList.remove('r')
  } else {
    cell.classList.remove('ch')
  }
  undo.push(cell)
  redoBtn.removeAttribute('disabled')
  if(!moves.length) {
    undoBtn.setAttribute('disabled', 'disabled')
    return
  }
}

cells.forEach((cell, idx) => {
  cell.onclick = () => {
    if(finish) return false
    finish = cellCliker(cell, idx)
    if(finish) {
      wonTitle.classList.remove('hidden')
      if(!finish.winner) {
        wonMessage.innerHTML = "It's a draw!"
      } else if (finish.winner === 'ch') {
        wonMessage.innerHTML = 'Crosses won!'
      } else if (finish.winner === 'r') {
        wonMessage.innerHTML = 'Toes won!'
      }
      const { winnerCombination } = finish
      let comb = winCombination2.get(winnerCombination)
      if(comb){
        switch (comb) {
          case 'h-top':
            winnerCombination.forEach((el) => {
                cells[+el].classList.add('win', 'horizontal')
            })
            break;
          case 'h-mid':
            winnerCombination.forEach((el) => {
              cells[+el].classList.add('win', 'horizontal')
            })
            break;
          case 'h-bot':
            winnerCombination.forEach((el) => {
              cells[+el].classList.add('win', 'horizontal')
            })
            break;
          case 'v-left':
            winnerCombination.forEach((el) => {
              cells[+el].classList.add('win', 'vertical')
            })
            break;
          case 'v-mid':
            winnerCombination.forEach((el) => {
              cells[+el].classList.add('win', 'vertical')
            })
            break;
          case 'v-rigth':
            winnerCombination.forEach((el) => {
              cells[+el].classList.add('win', 'vertical')
            })
            break;
          case 'lr-diag':
            winnerCombination.forEach((el) => {
              cells[+el].classList.add('win', 'diagonal-right')
            })
            break;
          case 'rl-diag':
            winnerCombination.forEach((el) => {
              cells[+el].classList.add('win', 'diagonal-left')
            })
            break;
          default:
            break;
        }
      }
    }
  }
})

const restart = () => {
  moves = [];
  redo = []
  undo = []
  cells.forEach((cell, idx) => {
    cell.classList.remove('r', 'ch', 'win', 'vertical', 'diagonal-left', 'diagonal-right', 'horizontal')
  })
  undoBtn.setAttribute('disabled', 'disabled')
  redoBtn.setAttribute('disabled', 'disabled')
  wonTitle.classList.add('hidden')
  wonMessage.innerHTML = ""
  finish = null
}

restartBtn.onclick = () => restart()
undoBtn.onclick = () => moveBack()
redoBtn.onclick = () => moveForward()
