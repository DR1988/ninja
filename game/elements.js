const cells = [...Array(9)].map(Number).map((el, idx) => {
    return document.getElementById(`c-${idx}`)
  }
)

const undoBtn = document.getElementsByClassName('undo-btn btn')[0]
const redoBtn = document.getElementsByClassName('redo-btn btn')[0]

const wonTitle = document.getElementsByClassName('won-title')[0]
const wonMessage = document.getElementsByClassName('won-message')[0]

const restartBtn = document.getElementsByClassName('restart-btn btn')[0]

export default {
  cells,
  undoBtn,
  redoBtn,
  restartBtn,
  wonTitle,
  wonMessage,
}